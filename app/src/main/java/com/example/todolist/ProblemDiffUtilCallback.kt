package com.example.todolist

import androidx.recyclerview.widget.DiffUtil
import com.example.todolist.model.Problem

class ProblemDiffUtilCallback(
    private val oldList: List<Problem>,
    private val newList: List<Problem>
) : DiffUtil.Callback() {
    override fun getOldListSize(): Int = oldList.size

    override fun getNewListSize(): Int = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val problemId1 = oldList[oldItemPosition].id
        val problemId2 = newList[newItemPosition].id
        return problemId1 == problemId2
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val problem1 = oldList[oldItemPosition]
        val problem2 = newList[newItemPosition]
        return problem1.title == problem2.title
    }
}