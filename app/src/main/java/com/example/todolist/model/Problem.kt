package com.example.todolist.model

data class Problem(val id: Long?=null, val title: String)