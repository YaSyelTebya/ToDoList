package com.example.todolist.model

data class ProblemStep(
    val stepId: Long? = null,
    val problemId: Long,
    val stepDescription: String,
    val isDone: Boolean = false
)