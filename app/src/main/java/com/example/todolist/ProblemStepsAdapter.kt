package com.example.todolist

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.todolist.databinding.ProblemStepListItemBinding
import com.example.todolist.model.ProblemStep

class ProblemStepsAdapter(
    private val problemSteps: MutableList<ProblemStep>,
    private val problemStepsManipulator: ProblemStepsManipulator
) : RecyclerView.Adapter<ProblemStepsAdapter.ProblemStepViewHolder>() {
    private lateinit var preferences: ColorsSharedPrefs
    class ProblemStepViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val binding: ProblemStepListItemBinding = ProblemStepListItemBinding.bind(view)
        val problemStepDescription: TextView  = binding.problemStepDescription
        val editButton: Button = binding.editProblemStepButton
        val deleteButton: Button = binding.deleteProblemStepButton
        val isDoneCheckbox: CheckBox = binding.isDoneCheckbox
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProblemStepViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.problem_step_list_item, parent, false)
        preferences = ColorsSharedPrefs(parent.context)

        return ProblemStepViewHolder(view)
    }

    override fun getItemCount(): Int = problemSteps.size

    override fun onBindViewHolder(holder: ProblemStepViewHolder, position: Int) {
        val problemStep = problemSteps[position]
        holder.problemStepDescription.text = problemStep.stepDescription

        val textColor = if (problemStep.isDone) {
            preferences.finishedColor
        } else {
            preferences.unfinishedColor
        }

        holder.problemStepDescription.setTextColor(Color.parseColor(textColor))

        holder.editButton.setOnClickListener {
            problemStepsManipulator.onEditProblemStep(problemStep)
        }

        holder.deleteButton.setOnClickListener {
            problemStepsManipulator.onDeleteProblemStep(problemStep)
        }

        holder.isDoneCheckbox.isChecked = problemStep.isDone

        holder.isDoneCheckbox.setOnCheckedChangeListener { _, isChecked ->
            problemStepsManipulator.onCheckProblemStep(problemStep.copy(isDone = isChecked))
        }
    }

    fun setNewProblemStepsList(newProblemSteps: List<ProblemStep>) {
        val diffUtilCallback = ProblemStepDiffUtilCallback(problemSteps, newProblemSteps)
        val diffProblems = DiffUtil.calculateDiff(diffUtilCallback)
        problemSteps.clear()
        problemSteps.addAll(newProblemSteps)
        diffProblems.dispatchUpdatesTo(this)
    }

    interface ProblemStepDeleter {
        fun onDeleteProblemStep(problemStep: ProblemStep)
    }

    interface ProblemStepEditor {
        fun onEditProblemStep(problemStep: ProblemStep)
    }

    interface ProblemStepChecker {
        fun onCheckProblemStep(problemStep: ProblemStep)
    }

    interface ProblemStepsManipulator : ProblemStepDeleter, ProblemStepEditor, ProblemStepChecker
}