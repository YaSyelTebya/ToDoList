package com.example.todolist

import com.example.todolist.model.Problem

sealed class OperationsWithProblems

data class DeleteProblem(val problemId: Long): OperationsWithProblems()

data class EditProblem(val problem: Problem): OperationsWithProblems()

data class AddProblem(val problem: Problem): OperationsWithProblems()