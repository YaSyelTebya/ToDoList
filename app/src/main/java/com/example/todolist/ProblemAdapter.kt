package com.example.todolist

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.todolist.model.Problem

class ProblemAdapter(
    private val problems: MutableList<Problem>,
    private val problemManipulator: ProblemManipulator
) : RecyclerView.Adapter<ProblemAdapter.ProblemViewHolder>() {
    class ProblemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val problemTitle: TextView
        val editButton: Button
        val deleteButton: Button

        init {
            problemTitle = view.findViewById(R.id.problem_title)
            editButton = view.findViewById(R.id.edit_button)
            deleteButton = view.findViewById(R.id.delete_button)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProblemViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.problem_list_item, parent, false)
        return ProblemViewHolder(view)
    }

    override fun getItemCount(): Int = problems.size

    override fun onBindViewHolder(holder: ProblemViewHolder, position: Int) {
        val problem = problems[position]
        holder.problemTitle.text = problem.title

        holder.problemTitle.setOnClickListener {
            problemManipulator.onProblemDetailsRequest(problem)
        }

        holder.editButton.setOnClickListener {
            problemManipulator.onProblemEdit(problem)
        }

        holder.deleteButton.setOnClickListener {
            problemManipulator.onDeleteProblem(problem)
        }
    }

    fun setNewProblemsList(newProblems: List<Problem>) {
        val diffUtilCallback = ProblemDiffUtilCallback(problems, newProblems)
        val diffProblems = DiffUtil.calculateDiff(diffUtilCallback)
        problems.clear()
        problems.addAll(newProblems)
        diffProblems.dispatchUpdatesTo(this)
    }

    interface ProblemDeleter {
        fun onDeleteProblem(problem: Problem)
    }

    interface ProblemEditor {
        fun onProblemEdit(problem: Problem)
    }

    interface ProblemDetailProvider {
        fun onProblemDetailsRequest(problem: Problem)
    }

    interface ProblemManipulator : ProblemDeleter, ProblemEditor, ProblemDetailProvider

    private companion object {
        const val TAG = "ProblemAdapter"
    }
}