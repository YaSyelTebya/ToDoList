package com.example.todolist

import com.example.todolist.model.ProblemStep

sealed class OperationsWithProblemSteps

data class EditProblemStep(val problemStep: ProblemStep) : OperationsWithProblemSteps()

data class DeleteProblemStep(val problemStepId: Long) : OperationsWithProblemSteps()

data class AddProblemStep(val problemStep: ProblemStep) : OperationsWithProblemSteps()