package com.example.todolist

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.graphics.ColorUtils
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.todolist.databinding.ActivityMainBinding
import com.example.todolist.db.ToDoListDataSource
import com.example.todolist.model.Problem
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class MainActivity : ProblemAdapter.ProblemManipulator,
    ProblemFormDialogListener,
    AppCompatActivity() {
    private lateinit var dataSource: ToDoListDataSource
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)

        setContentView(binding.root)
        setSupportActionBar(binding.myToolbar)

        dataSource = ToDoListDataSource(this)
        dataSource.open()

        binding.addNewProblemButton.setOnClickListener {
            openProblemForm(Problem(title = ""), getString(R.string.add))
        }

        val problems = dataSource.getAllProblems()

        val problemAdapter = ProblemAdapter(problems, this)

        binding.problemsList.adapter = problemAdapter
        binding.problemsList.layoutManager = LinearLayoutManager(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        dataSource.close()
    }

    override fun onReceiveProblemFromDialog(problem: Problem) {
        problem.id ?: return performProblemOperation(AddProblem(problem))
        performProblemOperation(EditProblem(problem))
    }

    private fun addNewProblem(problem: Problem) = dataSource.addProblem(problem)

    private fun editProblem(problem: Problem) = dataSource.editProblem(problem)

    private fun deleteProblem(problemId: Long) = dataSource.deleteProblem(problemId)

    private fun giveFeedbackOnOperationResult(isSuccessfullyCompleted: Boolean) {
        val feedbackMessageResource: Int = if (isSuccessfullyCompleted) {
            R.string.success
        } else {
            R.string.failed
        }
        Toast.makeText(this, feedbackMessageResource, Toast.LENGTH_LONG).show()
    }

    override fun onDeleteProblem(problem: Problem) {
        performProblemOperation(DeleteProblem(problem.id!!))
    }

    override fun onProblemEdit(problem: Problem) {
        openProblemForm(problem, getString(R.string.edit))
    }

    override fun onProblemDetailsRequest(problem: Problem) {
        val intent = Intent(this, ProblemStepsActivity::class.java)
        intent.putExtra(ProblemStepsActivity.PROBLEM_ID_KEY, problem.id)
        startActivity(intent)
    }

    private fun onProblemsChanges() {
        val recyclerAdapter = binding.problemsList.adapter as ProblemAdapter
        recyclerAdapter.setNewProblemsList(dataSource.getAllProblems())
    }

    private fun performProblemOperation(problemOperation: OperationsWithProblems) {
        lifecycleScope.launch {
            val isSuccess: Boolean = withContext(Dispatchers.IO) {
                 when (problemOperation) {
                    is DeleteProblem -> {
                        val result = deleteProblem(problemOperation.problemId)
                        result == 1
                    }

                    is AddProblem -> {
                        addNewProblem(problemOperation.problem)
                    }

                    is EditProblem -> {
                        val result = editProblem(problemOperation.problem)
                        result == 1
                    }
                }
            }
            giveFeedbackOnOperationResult(isSuccess)
            onProblemsChanges()
        }
    }

    private fun openProblemForm(problem: Problem, positiveActionButtonText: String) {
        val manipulateProblemDialog = ProblemFormDialog(problem, positiveActionButtonText)
        manipulateProblemDialog.show(supportFragmentManager, PROBLEM_FORM_TAG)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.action_bar_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when(item.itemId) {
        R.id.settings -> {
            startActivity(Intent(this, ColorSettingsActivity::class.java))
            true
        }
        else -> {
            throw IllegalStateException()
        }
    }

    private companion object {
        const val PROBLEM_FORM_TAG = "problem_form_tag"
    }
}