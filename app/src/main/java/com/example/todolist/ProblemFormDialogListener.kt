package com.example.todolist

import com.example.todolist.model.Problem

interface ProblemFormDialogListener {
    fun onReceiveProblemFromDialog(problem: Problem)
}