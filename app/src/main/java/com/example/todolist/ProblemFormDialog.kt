package com.example.todolist

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.example.todolist.databinding.ManipulateProblemDialogBinding
import com.example.todolist.model.Problem

class ProblemFormDialog(
    private val problem: Problem,
    private val positiveActionButtonText: String
) : DialogFragment() {
    private lateinit var binding: ManipulateProblemDialogBinding
    private lateinit var listener: ProblemFormDialogListener

    override fun onAttach(context: Context) {
        super.onAttach(context)

        listener = context as ProblemFormDialogListener
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        binding = ManipulateProblemDialogBinding.inflate(layoutInflater)
        binding.problemTitleInput.setText(problem.title)

        val builder: AlertDialog.Builder = AlertDialog.Builder(requireActivity())
            .setView(binding.root)
            .setPositiveButton(positiveActionButtonText) { _, _ ->
                listener.onReceiveProblemFromDialog(
                    problem.copy(title = binding.problemTitleInput.text.toString())
                )
            }
            .setNegativeButton("Cancel") { dialog, _ ->
                dialog.cancel()
            }

        return builder.create()
    }
}