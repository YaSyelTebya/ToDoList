package com.example.todolist

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import androidx.annotation.ColorInt

class ColorsSharedPrefs(private val context: Context) {
    private val unfinishedColorKey = context.getString(R.string.unfinished_color_key)
    private val finishedColorKey = context.getString(R.string.finished_color_key)
    private val finishedDefaultColor = context.getString(R.string.green)
    private val unfinishedDefaultColor = context.getString(R.string.red)
    private val preferences: SharedPreferences
        get() = context.getSharedPreferences(COLORS_SETTINGS, MODE_PRIVATE)

    var finishedColor: String
        get() = getColor(finishedColorKey, finishedDefaultColor)
        set(value) = setColor(value, finishedColorKey)

    var unfinishedColor: String
        get() = getColor(unfinishedColorKey, unfinishedDefaultColor)
        set(value) = setColor(value, unfinishedColorKey)

    private fun getColor(stateKey: String, defaultColor: String) =
        preferences.getString(stateKey, defaultColor)!!

    private fun setColor(newColor: String, stateKey: String) =
        preferences.edit()
            .putString(stateKey, newColor)
            .apply()

    companion object {
        const val COLORS_SETTINGS = "colors_settings"
    }
}