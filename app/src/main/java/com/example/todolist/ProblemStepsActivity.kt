package com.example.todolist

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.todolist.databinding.ActivityProblemStepsBinding
import com.example.todolist.db.ProblemStepsDataSource
import com.example.todolist.db.ToDoListDataSource
import com.example.todolist.model.ProblemStep
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlin.properties.Delegates

class ProblemStepsActivity : AppCompatActivity(), ProblemStepsAdapter.ProblemStepsManipulator,
    ProblemStepFormDialog.ProblemStepFormDialogListener {
    private lateinit var binding: ActivityProblemStepsBinding
    private lateinit var dataSource: ProblemStepsDataSource
    private var problemId by Delegates.notNull<Long>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityProblemStepsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val bundle = intent.extras!!
        problemId = bundle.getLong(PROBLEM_ID_KEY)

        dataSource = ToDoListDataSource(this)

        dataSource.open()

        val problemSteps = getProblemSteps(problemId)

        binding.problemsDetailsList.adapter =
            ProblemStepsAdapter(problemSteps, this)

        binding.problemsDetailsList.layoutManager = LinearLayoutManager(this)

        binding.addNewProblemStepButton.setOnClickListener {
            onCreateProblemStep(problemId)
        }
    }

    override fun onPause() {
        dataSource.close()
        super.onPause()
    }

    override fun onDeleteProblemStep(problemStep: ProblemStep) {
        performProblemStepOperation(DeleteProblemStep(problemStep.stepId!!))
    }

    override fun onEditProblemStep(problemStep: ProblemStep) {
        openProblemStepForm(problemStep, getString(R.string.edit))
    }

    override fun onCheckProblemStep(problemStep: ProblemStep) {
        performProblemStepOperation(EditProblemStep(problemStep))
    }

    private fun onCreateProblemStep(problemId: Long) {
        val problemStep = ProblemStep(problemId = problemId, stepDescription = "")
        openProblemStepForm(problemStep, "Add")
    }

    private fun openProblemStepForm(problemStep: ProblemStep, actionName: String) {
        val problemStepForm = ProblemStepFormDialog(problemStep, actionName)
        problemStepForm.show(supportFragmentManager, null)
    }

    override fun onReceive(problemStep: ProblemStep) {
        problemStep.stepId ?: return performProblemStepOperation(AddProblemStep(problemStep))
        performProblemStepOperation(EditProblemStep(problemStep))
    }

    private fun performProblemStepOperation(problemStepOperation: OperationsWithProblemSteps) {
        lifecycleScope.launch {
            val isSuccess: Boolean = withContext(Dispatchers.IO) {
                when (problemStepOperation) {
                    is DeleteProblemStep -> {
                        val result: Int = deleteProblemStep(problemStepOperation.problemStepId)
                        result == 1
                    }

                    is AddProblemStep -> {
                        createNewProblemStep(problemStepOperation.problemStep)
                    }

                    is EditProblemStep -> {
                        val result = editProblemStep(problemStepOperation.problemStep)
                        result == 1
                    }
                }
            }
            operationFeedback(isSuccess)
            onProblemStepChanges()
        }
    }

    private fun getProblemSteps(problemId: Long): MutableList<ProblemStep> =
        dataSource.getAllProblemSteps(problemId)

    private fun createNewProblemStep(problemStep: ProblemStep) : Boolean =
        dataSource.addProblemStep(problemStep)

    private fun editProblemStep(problemStep: ProblemStep): Int =
        dataSource.editProblemStep(problemStep)

    private fun deleteProblemStep(problemStepId: Long) : Int =
        dataSource.deleteProblemStep(problemStepId)

    private fun onProblemStepChanges() {
        val recyclerAdapter = binding.problemsDetailsList.adapter as ProblemStepsAdapter
        recyclerAdapter.setNewProblemStepsList(dataSource.getAllProblemSteps(problemId))
    }

    private fun operationFeedback(isSuccess: Boolean) {
        val feedbackMessageResource: Int = if (isSuccess) {
            R.string.success
        } else {
            R.string.failed
        }
        Toast.makeText(this, feedbackMessageResource, Toast.LENGTH_LONG).show()
    }

    companion object {
        const val PROBLEM_ID_KEY = "problem_id_key"
    }
}