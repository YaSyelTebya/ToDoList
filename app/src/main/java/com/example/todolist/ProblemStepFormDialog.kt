package com.example.todolist

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.example.todolist.databinding.ProblemStepFormDialogBinding
import com.example.todolist.model.ProblemStep

class ProblemStepFormDialog(
    private val problemStep: ProblemStep,
    private val positiveActionButtonText: String
) : DialogFragment() {
    private lateinit var binding: ProblemStepFormDialogBinding
    private lateinit var listener: ProblemStepFormDialogListener

    override fun onAttach(context: Context) {
        super.onAttach(context)

        listener = context as ProblemStepFormDialogListener
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        binding = ProblemStepFormDialogBinding.inflate(layoutInflater)
        binding.problemStepDescriptionInput.setText(problemStep.stepDescription)

        val builder: AlertDialog.Builder = AlertDialog.Builder(requireActivity())
            .setView(binding.root)
            .setPositiveButton(positiveActionButtonText) { _, _ ->
                listener.onReceive(
                    problemStep.copy(
                        stepDescription = binding.problemStepDescriptionInput.text.toString()
                    )
                )
            }
            .setNegativeButton("Cancel") { dialog, _ ->
                dialog.cancel()
            }

        return builder.create()
    }

    interface ProblemStepFormDialogListener {
        fun onReceive(problemStep: ProblemStep)
    }
}