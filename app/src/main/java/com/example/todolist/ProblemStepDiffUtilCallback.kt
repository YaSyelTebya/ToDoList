package com.example.todolist

import androidx.recyclerview.widget.DiffUtil
import com.example.todolist.model.ProblemStep

class ProblemStepDiffUtilCallback(
    private val oldProblemSteps: List<ProblemStep>,
    private val newProblemSteps: List<ProblemStep>
) : DiffUtil.Callback() {
    override fun getOldListSize(): Int = oldProblemSteps.size

    override fun getNewListSize(): Int = newProblemSteps.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
        oldProblemSteps[oldItemPosition].stepId == newProblemSteps[newItemPosition].stepId

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
        oldProblemSteps[oldItemPosition] == newProblemSteps[newItemPosition]
}