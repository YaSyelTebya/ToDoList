package com.example.todolist.db

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class SQLiteDatabaseOpenHelper(context: Context) : SQLiteOpenHelper(
    context,
    DATABASE_NAME,
    null,
    VERSION_NUMBER
) {
    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL("PRAGMA foreign_keys = ON")
        db?.execSQL(ToDoContract.CREATE_PROBLEM_TABLE_QUERY)
        db?.execSQL(ToDoContract.CREATE_PROBLEM_STEP_TABLE_QUERY)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.execSQL(ToDoContract.DROP_PROBLEM_TABLE_QUERY)
        db?.execSQL(ToDoContract.DROP_PROBLEM_STEP_TABLE_QUERY)

        onCreate(db)
    }

    private companion object {
        const val DATABASE_NAME = "todo_list.db"
        const val VERSION_NUMBER = 8
    }
}