package com.example.todolist.db

import com.example.todolist.model.ProblemStep

interface ProblemStepsDataSource : DataSource {
    fun getAllProblemSteps(problemId: Long): MutableList<ProblemStep>

    fun editProblemStep(problemStep: ProblemStep): Int

    fun deleteProblemStep(problemStepId: Long): Int

    fun addProblemStep(problemStep: ProblemStep): Boolean
}