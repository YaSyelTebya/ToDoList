package com.example.todolist.db

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.util.Log
import com.example.todolist.model.Problem
import com.example.todolist.model.ProblemStep

class ToDoListDataSource(context: Context) : ProblemStepsDataSource, ProblemDataSource {
    private var dbHelper: SQLiteDatabaseOpenHelper = SQLiteDatabaseOpenHelper(context)
    private lateinit var database: SQLiteDatabase

    override fun open() {
        database = dbHelper.writableDatabase
        Log.d(TAG, "Database opened")
    }

    override fun close() {
        dbHelper.close()
        Log.d(TAG, "Database closed")
    }

    override fun addProblem(problem: Problem): Boolean {
        val values = ContentValues()
        values.put(ToDoContract.Problem.COLUMN_TITLE, problem.title)

        val problemId: Long = database
            .insert(ToDoContract.Problem.TABLE_NAME, "", values)

        return isSuccessfullyInserted(problemId)
    }

    override fun editProblem(problem: Problem): Int {
        val newProblemTitleValues = ContentValues()
        newProblemTitleValues.put(ToDoContract.Problem.COLUMN_TITLE, problem.title)

        return database.update(
            ToDoContract.Problem.TABLE_NAME,
            newProblemTitleValues,
            "${ToDoContract.Problem._ID} = ?",
            arrayOf(problem.id!!.toString())
        )
    }

    override fun deleteProblem(problemId: Long) = database.delete(
        ToDoContract.Problem.TABLE_NAME,
        "${ToDoContract.Problem._ID} = ?",
        arrayOf(problemId.toString())
    )

    override fun getAllProblems(): MutableList<Problem> {
        val cursor = database.query(
            false,
            ToDoContract.Problem.TABLE_NAME,
            arrayOf(ToDoContract.Problem._ID, ToDoContract.Problem.COLUMN_TITLE),
            null,
            null,
            null,
            null,
            null,
            null
        )

        val problems = mutableListOf<Problem>()

        cursor.use {
            val idColumnIndex = it.getColumnIndexOrThrow(ToDoContract.Problem._ID)
            val titleColumnIndex = it.getColumnIndexOrThrow(ToDoContract.Problem.COLUMN_TITLE)

            var id: Long
            var title: String
            var problem: Problem

            while (cursor.moveToNext()) {
                id = it.getLong(idColumnIndex)
                title = it.getString(titleColumnIndex)
                problem = Problem(id, title)
                problems.add(problem)
            }
        }
        return problems
    }

    override fun getAllProblemSteps(problemId: Long): MutableList<ProblemStep> {
        val cursor = database.query(
            false,
            ToDoContract.ProblemStep.TABLE_NAME,
            arrayOf(
                ToDoContract.ProblemStep._ID,
                ToDoContract.ProblemStep.COLUMN_PROBLEM_ID,
                ToDoContract.ProblemStep.COLUMN_STEP_DESCRIPTION,
                ToDoContract.ProblemStep.COLUMN_IS_DONE
            ),
            "${ToDoContract.ProblemStep.COLUMN_PROBLEM_ID} = ?",
            arrayOf(problemId.toString()),
            null,
            null,
            null,
            null
        )

        val problemSteps = mutableListOf<ProblemStep>()

        cursor.use {
            val idColumnIndex = it.getColumnIndexOrThrow(ToDoContract.ProblemStep._ID)
            val problemIdColumnIndex = it
                .getColumnIndexOrThrow(ToDoContract.ProblemStep.COLUMN_PROBLEM_ID)
            val stepDescriptionColumnIndex = it
                .getColumnIndexOrThrow(ToDoContract.ProblemStep.COLUMN_STEP_DESCRIPTION)
            val isDoneColumnIndex = it
                .getColumnIndexOrThrow(ToDoContract.ProblemStep.COLUMN_IS_DONE)

            var id: Long
            var parentId: Long
            var description: String
            var isDone: Boolean

            while (cursor.moveToNext()) {
                id = it.getLong(idColumnIndex)
                parentId = it.getLong(problemIdColumnIndex)
                description = it.getString(stepDescriptionColumnIndex)
                isDone = ToDoContract.toBoolean(it.getLong(isDoneColumnIndex))
                val problemStep = ProblemStep(id, parentId, description, isDone)
                problemSteps.add(problemStep)
            }
        }

        return problemSteps
    }

    override fun editProblemStep(problemStep: ProblemStep): Int {
        val values = ContentValues()
        values.put(ToDoContract.ProblemStep.COLUMN_IS_DONE, problemStep.isDone)
        values.put(ToDoContract.ProblemStep.COLUMN_STEP_DESCRIPTION, problemStep.stepDescription)

        return database.update(
            ToDoContract.ProblemStep.TABLE_NAME,
            values,
            "${ToDoContract.ProblemStep._ID} = ?",
            arrayOf(problemStep.stepId!!.toString())
        )
    }

    override fun deleteProblemStep(problemStepId: Long): Int =
        database.delete(
            ToDoContract.ProblemStep.TABLE_NAME,
            "${ToDoContract.ProblemStep._ID} = ?",
            arrayOf(problemStepId.toString())
        )

    override fun addProblemStep(problemStep: ProblemStep): Boolean {
        val values = ContentValues()
        values.put(ToDoContract.ProblemStep.COLUMN_PROBLEM_ID, problemStep.problemId)
        values.put(ToDoContract.ProblemStep.COLUMN_STEP_DESCRIPTION, problemStep.stepDescription)
        values.put(ToDoContract.ProblemStep.COLUMN_IS_DONE, problemStep.isDone)
        val insertedId = database.insert(
            ToDoContract.ProblemStep.TABLE_NAME,
            "",
            values
        )
        return isSuccessfullyInserted(insertedId)
    }

    private fun isSuccessfullyInserted(insertedColumnId: Long): Boolean =
        insertedColumnId != FAILED_TO_INSERT_ID

    private companion object {
        const val TAG = "DB data sauce"
        const val FAILED_TO_INSERT_ID = -1L
    }
}