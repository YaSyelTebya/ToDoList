package com.example.todolist.db

interface DataSource {
    fun open()

    fun close()
}