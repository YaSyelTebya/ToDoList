package com.example.todolist.db

import android.provider.BaseColumns
import java.lang.IllegalArgumentException

object ToDoContract {
    const val TRUE = 1L
    const val  FALSE = 0L

    const val CREATE_PROBLEM_TABLE_QUERY = """
        CREATE TABLE ${Problem.TABLE_NAME} (
        ${Problem._ID} INTEGER PRIMARY KEY AUTOINCREMENT,
        ${Problem.COLUMN_TITLE} TEXT NOT NULL
        )
    """

    const val CREATE_PROBLEM_STEP_TABLE_QUERY = """
        CREATE TABLE ${ProblemStep.TABLE_NAME} (
        ${ProblemStep._ID} INTEGER PRIMARY KEY AUTOINCREMENT,
        ${ProblemStep.COLUMN_PROBLEM_ID} INTEGER NOT NULL,
        ${ProblemStep.COLUMN_STEP_DESCRIPTION} TEXT NOT NULL,
        ${ProblemStep.COLUMN_IS_DONE} INTEGER NOT NULL CHECK(
            ${ProblemStep.COLUMN_IS_DONE} = $FALSE OR
            ${ProblemStep.COLUMN_IS_DONE} = $TRUE),
        FOREIGN KEY(${ProblemStep.COLUMN_PROBLEM_ID}) 
            REFERENCES ${Problem.TABLE_NAME}(${Problem._ID}) ON DELETE RESTRICT
        )
    """ // TODO: make foreign key restriction work
    const val DROP_PROBLEM_TABLE_QUERY = """
        DROP TABLE ${Problem.TABLE_NAME}
    """

    const val DROP_PROBLEM_STEP_TABLE_QUERY = """
        DROP TABLE ${ProblemStep.TABLE_NAME}
    """

    fun toBoolean(isDone: Long): Boolean {
        if (isDone == TRUE) return true
        else if (isDone == FALSE) return false
        throw IllegalArgumentException("Provided value is not boolean")
    }

    object Problem : BaseColumns {
        const val TABLE_NAME = "problem"
        const val _ID = BaseColumns._ID
        const val COLUMN_TITLE = "title"
    }

    object ProblemStep : BaseColumns {
        const val TABLE_NAME = "problem_step"
        const val _ID = BaseColumns._ID
        const val COLUMN_PROBLEM_ID = "problem_id"
        const val COLUMN_STEP_DESCRIPTION = "step_description"
        const val COLUMN_IS_DONE = "is_done"
    }
}
