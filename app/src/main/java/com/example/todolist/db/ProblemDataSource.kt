package com.example.todolist.db

import android.content.ContentValues
import android.util.Log
import com.example.todolist.model.Problem

interface ProblemDataSource : DataSource {
    fun addProblem(problem: Problem): Boolean

    fun editProblem(problem: Problem): Int

    fun deleteProblem(problemId: Long): Int

    fun getAllProblems(): MutableList<Problem>
}